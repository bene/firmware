import display
import ujson
import os

FILENAME = 'nickname.txt'
FILENAME_ADV = 'nickname.json'


def write_nick(title, sub='', fg=None, bg=None, fg_sub=None, bg_sub=None):
    posy = 30
    if sub != '':
        posy = 18
    with display.open() as disp:
        disp.print(title, fg=fg, bg=bg, posx=80 - round(len(title) / 2 * 14), posy=posy)
        if sub != '':
            disp.print(sub, fg=fg_sub, bg=bg_sub, posx=80 - round(len(sub) / 2 * 14), posy=42)
        disp.update()
        disp.close()


def get_key(json, key, default):
    try:
        return json[key]
    except KeyError:
        return default


with display.open() as disp:
    disp.clear().update()

if FILENAME_ADV in os.listdir("."):
    f = open(FILENAME_ADV, 'r')
    try:
        c = ujson.loads(f.read())
        f.close()
        nick = get_key(c, 'nickname', 'no nick')
        sub = get_key(c, 'subtitle', '')
        fg_color = get_key(c, 'fg_color', [255, 255, 255])
        bg_color = get_key(c, 'bg_color', [0, 0, 0])
        fg_sub_color = get_key(c, 'fg_sub_color', [255, 255, 255])
        bg_sub_color = get_key(c, 'bg_sub_color', [0, 0, 0])
        background = get_key(c, 'background', [0, 0, 0])
        with display.open() as disp:
            disp.rect(0, 0, 160, 80, filled=True)
        write_nick(nick, sub, fg=fg_color, bg=bg_color, fg_sub=fg_sub_color, bg_sub=bg_sub_color)
    except ValueError:
        write_nick('invalid', sub='json')
else:
    if FILENAME not in os.listdir("."):
        write_nick('file not', sub='found')
    else:
        f = open(FILENAME, 'r')
        nick = f.read()
        f.close()
        if len(nick) > 11:
            write_nick('name too', sub='long')
        else:
            write_nick(nick)
